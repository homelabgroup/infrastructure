module "harbor" {
  // Required Variables
  source              = "git::https://github.com/shakir85/terraform_modules.git//proxmox/vm?ref=v0.1.0"
  proxmox_node_name   = "pve1"
  disk_name           = "sdd"
  ssh_public_key_path = var.id_rsa_pub
  username            = "shakir"
  hostname            = "harbor"
  timezone            = "America/Los_Angeles"
  cloud_image_info    = ["sdc", "debian-12-generic-amd64.qcow2.img"]
  disk_size           = 256
  tags                = ["prod"]
  cores               = 4
  sockets             = 1
  memory              = 8192
}

output "module_outputs" {
  value = module.harbor
}
