module "ubuntu_24-04_cloudimg_amd64" {
  // Required Variables
  source            = "git::https://github.com/shakir85/terraform_modules.git//proxmox/cloud-img-download?ref=v0.2.1"
  proxmox_node_name = "pve1"
  cloud_image_url   = "https://cloud-images.ubuntu.com/noble/current/noble-server-cloudimg-amd64.img"
  storage_pool      = "sdc"
}
