module "zabbix" {
  # Required Variables
  source              = "git::https://github.com/shakir85/terraform_modules.git//proxmox/vm?ref=v0.2.0"
  proxmox_node_name   = "pve1"
  disk_name           = "sdd"
  ssh_public_key_path = var.id_rsa_pub
  username            = "shakir"
  hostname            = "zabbix"
  timezone            = "America/Los_Angeles"
  cloud_image_info    = ["sdc", "debian-12-generic-amd64.qcow2.img"]
  tags                = ["prod", "debian12", "monitoring"]
  description         = "Managed by Terraform."
  memory              = 8192
  cores               = 2
  sockets             = 1
  disk_size           = 90
  enable_guest_agent  = true
}

# Print any output block from the main module
output "module_outputs" {
  value = module.zabbix
}
