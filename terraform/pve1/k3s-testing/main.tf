module "k3s-ctrl" {
  # Required Variables
  source              = "git::https://github.com/shakir85/terraform_modules.git//proxmox/vm?ref=v0.2.0"
  proxmox_node_name   = "pve1"
  disk_name           = "sdd"
  ssh_public_key_path = var.id_rsa_pub
  username            = "shakir"
  hostname            = "k3s-ctrl"
  timezone            = "America/Los_Angeles"
  cloud_image_info    = ["sdc", "noble-server-cloudimg-amd64.img.img"]
  tags                = ["test", "ubuntu2404", "k8s"]
  description         = "Managed by Terraform."
  memory              = 4096
  cores               = 2
  sockets             = 1
  disk_size           = 50
}

module "k3s-worker" {
  # Required Variables
  source              = "git::https://github.com/shakir85/terraform_modules.git//proxmox/vm?ref=v0.2.0"
  proxmox_node_name   = "pve1"
  disk_name           = "sdd"
  ssh_public_key_path = var.id_rsa_pub
  username            = "shakir"
  hostname            = "k3s-worker"
  timezone            = "America/Los_Angeles"
  cloud_image_info    = ["sdc", "noble-server-cloudimg-amd64.img.img"]
  tags                = ["test", "ubuntu2404", "k8s"]
  description         = "Managed by Terraform."
  memory              = 2048
  cores               = 2
  sockets             = 1
  disk_size           = 50
}

# Print any output block from the main module
output "k3s_ctrl-module_outputs" {
  value = module.k3s-ctrl
}

output "k3s_worker-module_outputs" {
  value = module.k3s-worker
}
