module "vault" {
  source              = "git::https://github.com/shakir85/terraform_modules.git//proxmox/lxc?ref=v0.2.0"
  node_name           = "pve1"
  hostname            = "vault"
  template_file_id    = "sdd:vztmpl/ubuntu-24.04-standard_24.04-2_amd64.tar.zst"
  disk_id             = "sdd"
  ssh_public_key_path = var.id_rsa_pub
  os_type             = "ubuntu"
  cpu_cores           = 1
  memory              = 1024
}

