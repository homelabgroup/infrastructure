#! /usr/bin/bash
# Source:
# How to use this script?
#   1. Add this script to: /usr/local/bin/disable_subscription_pop-up.sh
#   2. chmod 755 /usr/local/bin/disable_subscription_pop-up.sh
#   3. Run: disable_subscription_pop-up.sh
#
pop_up_file="/usr/share/javascript/proxmox-widget-toolkit/proxmoxlib.js"
base_string=".data.status.toLowerCase() !== 'active') {"
patched_string=".data.status.toLowerCase() !== 'active') { orig_cmd\(\); } else if ( false ) {"

# Disable pop-up
if grep --quiet "[[:space:]]*${base_string}$" ${pop_up_file} ; then
  sed --in-place=.back "s/${base_string}$/${patched_string}/" ${pop_up_file}
  restart_services=true
fi

# Restart required services if necessary
if [[ ${restart_services} == 'true' ]] ; then
  # Wait for web consoles to close before restarting the services
  while [[ $(ps -ef | grep termproxy | wc -l) -gt 1 ]] ; do
    sleep 1
  done
  for service in pveproxy proxmox-backup-proxy pmgproxy ; do
    if [[ $(systemctl is-active ${service}.service) == 'active' ]] ; then
      systemctl restart ${service}.service
    fi
  done
fi
